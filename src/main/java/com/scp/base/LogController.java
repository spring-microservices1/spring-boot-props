package com.scp.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LogController {
	
	private Logger _Log=LoggerFactory.getLogger(LogController.class);
	@Autowired
	private DBSettings dbSettings;

	@GetMapping("/props")
	public String printProps() {
		return dbSettings.getHost()+" "+dbSettings.getPort()+" "+dbSettings.getConnection().toString();
	}
}
