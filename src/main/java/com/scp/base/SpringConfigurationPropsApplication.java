package com.scp.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringConfigurationPropsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringConfigurationPropsApplication.class, args);
	}

}
